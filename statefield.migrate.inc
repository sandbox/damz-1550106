<?php

/**
 * @file
 * Support for processing statefields in Migrate.
 */


/**
 * Implement hook_migrate_api().
 */
function statefield_migrate_api() {
  $api = array(
    'api' => 2,
  );
  return $api;
}

class MigrateEntityStateFieldHandler extends MigrateFieldHandler {
  public function __construct() {
    $this->registerTypes(array('statefield'));
  }

  public function prepare($entity, array $field_info, array $instance, array $values) {
    if (isset($values['arguments'])) {
      $arguments = $values['arguments'];
      unset($values['arguments']);
    }
    else {
      $arguments = array();
    }

    // Setup the standard Field API array for saving.
    $delta = 0;
    foreach ($values as $value) {
      $return[LANGUAGE_NONE][$delta]['state'] = $value;
      $delta++;
    }
    if (!isset($return)) {
      $return = NULL;
    }
    return $return;
  }
}
