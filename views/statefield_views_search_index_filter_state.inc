<?php

class StateFieldViewsSearchIndexFilterState extends StateFieldViewsSearchIndexFilterMultiState {

  // Get value options.
  function get_value_options() {
    if (empty($this->definition['entity_type'])) {
      $this->value_options = array();
      return;
    }
    $field = field_info_field($this->definition['field_name']);
    $states = array();
    foreach ($field['bundles'][$this->definition['entity_type']] as $bundle) {
      $instance = field_info_instance($this->definition['field_name'], $this->definition['entity_type'], $bundle);
      $states += statefield_states($field, $instance, $this->definition['entity_type'], NULL, $GLOBALS['user']);
    }

    $this->value_options = array();
    foreach ($states as $state_name => $state_info) {
      $this->value_options[$state_name] = decode_entities(strip_tags($state_info['title']));
    }
  }

  function query() {
    if (!$this->value) {
      // Nothing to do.
      return;
    }

    foreach ($this->value as $value) {
      $this->query->condition($this->real_field, $value, $this->operator, $this->options['group']);
    }
  }

}

