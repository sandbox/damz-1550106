<?php

class statefield_views_filter_multistate extends views_handler_filter_in_operator {
  function get_value_options() {
    // Find the entity type from which this field is displayed.
    $base_table = $this->get_base_table();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if ($entity_info['base table'] == $base_table) {
        $field_entity_type = $entity_type;
        break;
      }
    }

    $this->value_options = array();

    if (empty($field_entity_type)) {
      return;
    }

    // Load states from each state field attached to the entity type.
    $states = array();
    $instances = field_info_instances($field_entity_type);
    foreach ($instances as $bundle => $instances) {
      foreach ($instances as $instance) {
        $field = field_info_field($instance['field_name']);
        if ($field['type'] !== 'statefield') {
          continue;
        }

        foreach (statefield_states($field, $instance, $field_entity_type, NULL, $GLOBALS['user']) as $state_name => $state_info) {
          $this->value_options[$instance['label']][$field['field_name'] . ':' . $state_name] = decode_entities(strip_tags($state_info['title']));
        }
      }
    }
  }

  function query() {
    if (!$this->value) {
      // Nothing to do.
      return;
    }

    // Remove the field prefix from values.
    $original_values = $this->value;
    foreach ($this->value as &$value) {
      // TODO: check that all the values share the same $field_name.
      list($field_name, $value) = explode(':', $value, 2);
    }

    // Join to the proper field table.
    $field = field_info_field($field_name);
    $field_table = _field_sql_storage_tablename($field);
    $field_column = $field['storage']['details']['sql'][FIELD_LOAD_CURRENT][$field_table]['state'];

    $this->ensure_my_table();
    $this->field_table = $this->query->ensure_table($field_table, $this->relationship);

    $this->table_alias = $this->field_table;
    $this->real_field = $field_column;

    parent::query();
    // Restore the original values.
    $this->value = $original_values;
  }

  /**
   * Set the base_table and base_table_alias.
   */
  function get_base_table() {
    if (!isset($this->base_table)) {
      // This base_table is coming from the entity not the field.
      $this->base_table = $this->view->base_table;

      // If the current field is under a relationship you can't be sure that the
      // base table of the view is the base table of the current field.
      // For example a field from a node author on a node view does have users as base table.
      if (!empty($this->options['relationship']) && $this->options['relationship'] != 'none') {
        $relationships = $this->view->display_handler->get_option('relationships');
        $options = $relationships[$this->options['relationship']];
        $data = views_fetch_data($options['table']);
        $this->base_table = $data[$options['field']]['relationship']['base'];
      }
    }

    return $this->base_table;
  }

}
