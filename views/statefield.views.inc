<?php

/**
 * Implements hook_field_views_data().
 */
function statefield_field_views_data($field) {
  $data = field_views_field_default_views_data($field);
  foreach ($data as $table_name => $table_data) {
    foreach ($table_data as $field_name => $field_data) {
      if (isset($field_data['filter'])) {
        $data[$table_name][$field_name]['filter']['handler'] = 'statefield_views_filter_state';
      }
    }
  }

  foreach ($field['bundles'] as $entity_type => $bundles) {
    $entity_info = entity_get_info($entity_type);
    $data[$entity_info['base table']]['multistate']['filter'] = array(
      'table' => $entity_info['base table'],
      'title' => t('Multistate'),
      'help' => t('Multistate field.'),
      'handler' => 'statefield_views_filter_multistate',
      'field_name' => $field['field_name'],
      'field_table' => _field_sql_storage_tablename($field),
      'field_column' => $field['storage']['details']['sql'][FIELD_LOAD_CURRENT][_field_sql_storage_tablename($field)]['state'],
      'allow empty' => TRUE,
    );
  }

  // Check if the search_api_views module is enabled.
  if (module_exists('search_api_views')) {
    foreach (search_api_index_load_multiple(FALSE) as $index) {
      $key = 'search_api_index_' . $index->machine_name;
      $entity_info = entity_get_info($index->item_type);
      $table = &$data[$key];
      $table['multistate']['filter'] = array(
        'table' => $entity_info['base table'],
        'title' => t('Multistate'),
        'help' => t('Multistate field.'),
        'handler' => 'StateFieldViewsSearchIndexFilterMultiState',
        'field_name' => $field['field_name'],
        'field_table' => _field_sql_storage_tablename($field),
        'field_column' => $field['storage']['details']['sql'][FIELD_LOAD_CURRENT][_field_sql_storage_tablename($field)]['state'],
        'allow empty' => TRUE,
      );
    }
  }
  return $data;
}

/**
 * Implements hook_views_data_alter().
 */
function statefield_views_data_alter(&$data) {
  if (module_exists('search_api_views')) {
    foreach (search_api_index_load_multiple(FALSE) as $index) {
      // Fill in base data.
      $key = 'search_api_index_' . $index->machine_name;
      $table = &$data[$key];
      $indexed_fields = $index->getFields();
      // Update filter handler for all indexed fields.
      foreach ($indexed_fields as $key => $field) {
        // Find in the key if it's a related field.
        $key_explode = explode(':', $key);
        $id = _entity_views_field_identifier($key, $table);
        if (isset($table[$id]['filter']) && isset($table[$id]['field']['field_name'])) {
          $field_info = field_info_field($table[$id]['field']['field_name']);
          if ($field_info['type'] == 'statefield' && $field_info['module'] == 'statefield') {
            $table[$id]['filter']['handler'] = 'StateFieldViewsSearchIndexFilterState';
            $table[$id]['filter']['field_name'] = $table[$id]['field']['field_name'];
            $table[$id]['filter']['entity_type'] = isset($key_explode[1]) ? $indexed_fields[$key_explode[0]]['entity_type'] : $table[$id]['field']['entity type'];
          }
        }
      }
    }
  }
}