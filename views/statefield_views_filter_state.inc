<?php

class statefield_views_filter_state extends views_handler_filter_in_operator {
  function get_value_options() {
    // Find the entity type from which this field is displayed.
    $base_table = $this->get_base_table();
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if ($entity_info['base table'] == $base_table) {
        $field_entity_type = $entity_type;
        break;
      }
    }

    if (empty($field_entity_type)) {
      $this->value_options = array();
      return;
    }

    $field = field_info_field($this->definition['field_name']);
    $states = array();
    foreach ($field['bundles'][$field_entity_type] as $bundle) {
      $instance = field_info_instance($this->definition['field_name'], $field_entity_type, $bundle);
      $states += statefield_states($field, $instance);
    }

    $this->value_options = array();
    foreach ($states as $state_name => $state_info) {
      $this->value_options[$state_name] = decode_entities(strip_tags($state_info['title']));
    }
  }

  /**
   * Set the base_table and base_table_alias.
   */
  function get_base_table() {
    if (!isset($this->base_table)) {
      // This base_table is coming from the entity not the field.
      $this->base_table = $this->view->base_table;

      // If the current field is under a relationship you can't be sure that the
      // base table of the view is the base table of the current field.
      // For example a field from a node author on a node view does have users as base table.
      if (!empty($this->options['relationship']) && $this->options['relationship'] != 'none') {
        $relationships = $this->view->display_handler->get_option('relationships');
        $options = $relationships[$this->options['relationship']];
        $data = views_fetch_data($options['table']);
        $this->base_table = $data[$options['field']]['relationship']['base'];
      }
    }
    return $this->base_table;
  }

}
