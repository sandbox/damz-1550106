<?php

class StateFieldViewsSearchIndexFilterMultiState extends views_handler_filter_in_operator {
  var $value_form_type = 'select';

  function get_value_options() {
    // Find the entity type from which this field is displayed.
    $base_table = $this->definition['table'];
    foreach (entity_get_info() as $entity_type => $entity_info) {
      if ($entity_info['base table'] == $base_table) {
        $field_entity_type = $entity_type;
        break;
      }
    }

    $this->value_options = array();

    if (empty($field_entity_type)) {
      return;
    }

    // Load states from each state field attached to the entity type.
    $states = array();
    $instances = field_info_instances($field_entity_type);
    foreach ($instances as $bundle => $instances) {
      foreach ($instances as $instance) {
        $field = field_info_field($instance['field_name']);
        if ($field['type'] !== 'statefield') {
          continue;
        }

        foreach (statefield_states($field, $instance, $field_entity_type, NULL, $GLOBALS['user']) as $state_name => $state_info) {
          $this->value_options[$instance['label']][$field['field_name'] . ':' . $state_name] = decode_entities(strip_tags($state_info['title']));
        }
      }
    }
  }

  /*
   * Override some default options;
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['operator']['default'] = '=';
    $options['value']['default'] = array();
    $options['expose']['contains']['reduce'] = array('default' => FALSE);

    return $options;
  }

  /**
   * Overrride operators.
   */
  function operators() {
    $operators = array(
      '=' => array(
        'title' => t('Is one of'),
        'short' => t('in'),
        'short_single' => t('='),
        'values' => 1,
      ),
      '<>' => array(
        'title' => t('Is not one of'),
        'short' => t('not in'),
        'short_single' => t('<>'),
        'values' => 1,
      ),
    );
    return $operators;
  }

  /**
    * Provide a list of options for the operator form.
    */
   function operator_options($which = 'title') {
     return array(
       '=' => t('Is one of'),
       '<>' => t('Is not one of'),
     );
   }

  function query() {
    if (!$this->value) {
      // Nothing to do.
      return;
    }

    foreach ($this->value as $value) {
      list($field_name, $value) = explode(':', $value, 2);
      $this->query->condition($field_name, $value, $this->operator, $this->options['group']);
    }
  }


  /**
   * We override the value_form function in order to only have the value we want.
   */
  function value_form(&$form, &$form_state) {
    $form['value'] = array();
    $options = array();

    if (empty($form_state['exposed'])) {
      // Add a select all option to the value form.
      $options = array('all' => t('Select all'));
    }

    $this->get_value_options();
    $options += $this->value_options;
    $default_value = (array) $this->value;

    $which = 'all';
    if (!empty($form['operator'])) {
      $source = ($form['operator']['#type'] == 'radios') ? 'radio:options[operator]' : 'edit-options-operator';
    }
    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];

      if (empty($this->options['expose']['use_operator']) || empty($this->options['expose']['operator_id'])) {
        // exposed and locked.
        $which = in_array($this->operator, $this->operator_values(1)) ? 'value' : 'none';
      }
      else {
        $source = 'edit-' . drupal_html_id($this->options['expose']['operator_id']);
      }

      if (!empty($this->options['expose']['reduce'])) {
        $options = $this->reduce_value_options();

        // Delete the unwanted values
        foreach($options as $key => $option){
          if( count($option) == 0 ) {
            unset($options[$key]);
          }
        }

        if (!empty($this->options['expose']['multiple']) && empty($this->options['expose']['required'])) {
          $default_value = array();
        }
      }

      if (empty($this->options['expose']['multiple'])) {
        if (empty($this->options['expose']['required']) && (empty($default_value) || !empty($this->options['expose']['reduce']))) {
          $default_value = 'All';
        }
        elseif (empty($default_value)) {
          $keys = array_keys($options);
          $default_value = array_shift($keys);
        }
        else {
          $copy = $default_value;
          $default_value = array_shift($copy);
        }
      }
    }

    if ($which == 'all' || $which == 'value') {
      $form['value'] = array(
        '#type' => $this->value_form_type,
        '#title' => $this->value_title,
        '#options' => $options,
        '#default_value' => $default_value,
        // These are only valid for 'select' type, but do no harm to checkboxes.
        '#multiple' => TRUE,
        '#size' => count($options) > 8 ? 8 : count($options),
      );
      if (!empty($form_state['exposed']) && !isset($form_state['input'][$identifier])) {
        $form_state['input'][$identifier] = $default_value;
      }

      if ($which == 'all') {
        if (empty($form_state['exposed']) && (in_array($this->value_form_type, array('checkbox', 'checkboxes', 'radios', 'select')))) {
          $form['value']['#prefix'] = '<div id="edit-options-value-wrapper">';
          $form['value']['#suffix'] = '</div>';
        }
        $form['value']['#dependency'] = array($source => $this->operator_values(1));
      }
    }
  }

}
