<?php

/**
 * Define states for a given field.
 */
function hook_statefield_state_info($field, $instance) {
  $states['draft'] = array(
    'title' => t('Draft'),
    'button title' => t('Save as draft'),
  );
  $states['published'] = array(
    'title' => t('Published'),
    'button title' => t('Save and publish'),
  );
  return $states;
}

/**
 * Alter the states defined for a given entity type.
 */
function hook_statefield_state_info_alter(&$states, $field, $instance) {
  
}

/**
 * Allow modules to alter the states dynamically, for a given entity and a given user.
 *
 * Are invalid in this hook:
 *  - adding states
 *  - changing the internal name of existing states
 *
 * @param &$states
 *   The list of states to alter, as returned from hook_statefield_state_info()
 *   and hook_statefield_state_info_alter().
 * @param $context
 *   Array of additional information available for the context:
 *   - 'field': The field definition.
 *   - 'instance': The instance definition.
 *   - 'entity_type': The type of the entity.
 *   - 'entity': The full entity object (optional).
 *   - 'account': The user interacting with the entity.
 */
function hook_statefield_state_info_dynamic_alter(&$states, $context) {

}
